package com.lwl.boot.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lwl.boot.entity.Item;
import com.lwl.boot.service.ItemService;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
@RestController
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;

	@GetMapping("add")
	public Object add() {
		for (int i = 0; i < 50; i++) {
			Item item = new Item();
			item.setValue(i);
			item.setIndate(new Date());
			itemService.insert(item);
		}
		return 0;
	}
}
