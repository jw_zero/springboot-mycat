package com.lwl.boot.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lwl.boot.entity.Users;
import com.lwl.boot.service.UsersService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
@RestController
@RequestMapping("/users")
public class UsersController {
	@Autowired
	private UsersService usersService;

	@GetMapping("add")
	public Object add() {
		for (int i = 0; i < 50; i++) {
			Users users = new Users();
			users.setIndate(new Date());
			users.setName(""+i);
			usersService.insert(users);
		}
		return 0;
	}
}

