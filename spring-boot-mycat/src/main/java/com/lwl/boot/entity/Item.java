package com.lwl.boot.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
@Getter
@Setter
public class Item extends Model<Item> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    private Integer value;
    private Date indate;

	@Override
	protected Serializable pkVal() {
		return id;
	}
}
