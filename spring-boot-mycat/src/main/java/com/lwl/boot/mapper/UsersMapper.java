package com.lwl.boot.mapper;

import com.lwl.boot.entity.Users;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
public interface UsersMapper extends BaseMapper<Users> {

}
