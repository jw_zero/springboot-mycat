package com.lwl.boot.service;

import com.lwl.boot.entity.Item;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
public interface ItemService extends IService<Item> {

}
