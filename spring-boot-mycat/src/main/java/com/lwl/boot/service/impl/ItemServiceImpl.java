package com.lwl.boot.service.impl;

import com.lwl.boot.entity.Item;
import com.lwl.boot.mapper.ItemMapper;
import com.lwl.boot.service.ItemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
@Service
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements ItemService {

}
