package com.lwl.boot.service.impl;

import com.lwl.boot.entity.Users;
import com.lwl.boot.mapper.UsersMapper;
import com.lwl.boot.service.UsersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jw
 * @since 2018-08-29
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {

}
